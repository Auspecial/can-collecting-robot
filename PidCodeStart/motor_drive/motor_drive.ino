#include <Metro.h> // Include the Metro library
#include <Servo.h>
#include <AutoPID.h>

#define OUTPUT_PIN A1

//main PID consts
#define OUTPUT_MIN_IR 0
#define OUTPUT_MAX_IR 180
#define KP_IR 0.15
#define KI_IR 0.0003
#define KD_IR 0

//left and right pid consts
#define RIGHT_KP 0.10
#define RIGHT_KI 0.0
#define RIGHT_KD 0.0
#define LEFT_KP 0.10
#define LEFT_KI 0.0
#define LEFT_KD 0.0

//rgb sensor
#define S0 4
#define S1 5
#define S2 6
#define S3 7
#define sensorOut 8

Metro serialMetro = Metro(250);  // Instantiate an instance
Metro sensorMetro = Metro(100);
double countLeft=0;
double countRight=0;

//will come from sensor
int detectedColour;

//will eventually come from state.
int colour = 0;

//-1 is reverse, 1 is forward
int dir = 1;

int targetPos = 1000;

//IR PID
double irRightAddition, irLeftAddition, driveLeftPidOutput, driveRightPidOutput;
double inputIR, setPointIR, outputFromIR;

//DRIVE PIDS
double driveSetPoint;
double outputRDrive;
double outputLDrive;


//rgb stuff
int frequency, redFreq, greenFreq, blueFreq;
bool sensorRed, sensorGreen, sensorBlue;

// servos
Servo servoLeft;
Servo servoRight;

int throttleLeft, throttleRight;

//pid loops
AutoPID sensorPID(&inputIR, &setPointIR, &outputFromIR, OUTPUT_MIN_IR, OUTPUT_MAX_IR, KP_IR, KI_IR, KD_IR);
AutoPID leftPid(&countLeft, &driveSetPoint, &outputLDrive, OUTPUT_MIN_IR, OUTPUT_MAX_IR, LEFT_KP, LEFT_KI, LEFT_KD);
AutoPID rightPid(&countRight, &driveSetPoint, &outputRDrive, OUTPUT_MIN_IR, OUTPUT_MAX_IR, RIGHT_KP, RIGHT_KI, RIGHT_KD);


void ISRleft(){
  if(digitalRead(22)==LOW){
    countLeft=countLeft+1;}
  else{countLeft=countLeft-1;}
}
void ISRright(){
  if(digitalRead(23)==HIGH){
    countRight=countRight+1;}
  else{countRight=countRight-1;}
}

bool inRange(int val, int minimum, int maximum)
{
  return ((minimum <= val) && (val <= maximum));
}


void checkColourSensor(){

  //red values
  digitalWrite(S2,LOW);
  digitalWrite(S3,LOW);
  redFreq = pulseIn(sensorOut, LOW);
  redFreq = map(redFreq, 130, 555, 0, 255);
  if (redFreq < 50 && (redFreq < redFreq && redFreq < blueFreq)){
    sensorRed = true;
    sensorBlue = false;
    sensorGreen = false;
    detectedColour = 0;
  }

  delay(100);

  // Setting Green filtered photodiodes to be read
  digitalWrite(S2,HIGH);
  digitalWrite(S3,HIGH);
  greenFreq = pulseIn(sensorOut, LOW);
  greenFreq = map(greenFreq, 190, 1300, 0, 255);
  if (greenFreq < 50 && (greenFreq < redFreq && greenFreq < blueFreq)){
    sensorGreen = true;
    sensorBlue = false;
    sensorRed = false;
    detectedColour = 1;
  }

  delay(100);

  // Setting Blue filtered photodiodes to be read
  digitalWrite(S2,LOW);
  digitalWrite(S3,HIGH);
  blueFreq = pulseIn(sensorOut, LOW);
  blueFreq = map(blueFreq, 55, 370, 0, 255);
  if (blueFreq < 50 && (blueFreq < greenFreq && blueFreq < redFreq)){
    sensorBlue = true;
    sensorGreen = false;
    sensorRed = false;
    detectedColour = 2;
  }

  delay(100);

}

void addLeftPidValues(){
  leftPid.run();
  //Serial.print(outputLDrive);
  throttleLeft += outputLDrive;
}
void addRightPidValues(){
  rightPid.run();
  //Serial.print(outputRDrive);
  throttleRight += outputRDrive;
}

//implements line following PID and adds values to drive
void addLinePidValues(int dir){
  irLeftAddition = 0;
  irRightAddition = 0;
  if (dir == 1){
    //front sensor
    inputIR = analogRead(A0);
  }else{
    //rear sensor
    inputIR = analogRead(A1);
  }
  Serial.print(inputIR);
  setPointIR = 2.5;
  sensorPID.run();
  double out = outputFromIR;
  if (out > 2.5){
    irRightAddition = out;
  }else if(out < 2.5){
    irLeftAddition = out;
  }
  throttleLeft += irLeftAddition;
  throttleRight += irRightAddition;
}


void setup(){
  driveSetPoint = targetPos;
  pinMode(OUTPUT_PIN, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(18), ISRleft, RISING);
  attachInterrupt(digitalPinToInterrupt(19), ISRright, RISING);
  pinMode(22,INPUT); // 18 and 22 come from left
  pinMode(23,INPUT); // 19 and 23 come from right
  pinMode(6,INPUT); //enable pin
  servoLeft.attach(4);  // attaches the servo on pin 9 to the servo object
  servoRight.attach(5);  // attaches the servo on pin 9 to the servo object

}

void loop() {

 if(true){ // put your main code here, to run repeatedly:
  if(true){ //moveCommandFromStateIsTrue

    //will come from state, 1,2,3 r,g,b
    int colourRequest = colour;
    if (colour == 0 && (countLeft > 0 || countRight > 0)){
      dir = -1;
    }else{
      dir = 1;
    }
    
    checkColourSensor();

    if (colourRequest == detectedColour){
      servoLeft.write(90);
      servoRight.write(90);
      return;
    }
    if (inRange(countLeft, driveSetPoint-10, driveSetPoint+10) || inRange(countRight, driveSetPoint-10, driveSetPoint+10)){
      Serial.print("At set point");
      servoLeft.write(90);
      servoRight.write(90);
      return;
    }

    //every 100 millis
    if (sensorMetro.check() == 1){
      Serial.print("every100millis");
      addLeftPidValues();
      addRightPidValues();
      addLinePidValues(dir);
      servoLeft.write(constrain(throttleLeft,0,180) * dir);
      servoRight.write(constrain(throttleRight,0,180) * dir);
    }

    
    
   }
  
  }else{ //power off stop robot
    servoLeft.write(90);
    servoRight.write(90);
  }
}
