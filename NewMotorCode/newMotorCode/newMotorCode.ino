#include <Metro.h> // Include the Metro library
#include <Servo.h>


#define FORWARD_SENSOR A0
#define REVERSE_SENSOR A1
//rgb sensor
#define S0 4
#define S1 5
#define S2 6
#define S3 7
#define sensorOut 8

//timers
Metro serialMetro = Metro(250);
Metro sensorMetro = Metro(100);

// servos
double countLeft=0;
double countRight=0;

Servo servoLeft;
Servo servoRight;

int throttleLeft, throttleRight;
int leftBaseSpeed = 140;
int rightBaseSpeed = 140;
int returnValue;

int rightMotorSpeed = 0;
int leftMotorSpeed = 0;

//will come from sensor
int detectedColour;

//will eventually come from state.
int colour = 0;

//-1 is reverse, 1 is forward
int dir = 1;

int driveSetPoint = 1000;
int targetPos = 1000;

//IR PID consts
int whiteTape = 863;
int blackTape = 1000;
int currentPosition = 0;
double kP = 0.1;
int error = 0;
double integral = 0;
double kI = 0.001;
double iZone = 10;
double derivative = 0;
double kD = 0.05;
double previousError = 0;
int outputSpeed = 0;
int mapLower = 90;
int mapUpper = 90;


//rgb stuff
int frequency, redFreq, greenFreq, blueFreq;
bool sensorRed, sensorGreen, sensorBlue;


void ISRleft(){
  if(digitalRead(22)==LOW){
    countLeft=countLeft+1;}
  else{countLeft=countLeft-1;}
}
void ISRright(){
  if(digitalRead(23)==HIGH){
    countRight=countRight+1;}
  else{countRight=countRight-1;}
}

bool inRange(int val, int minimum, int maximum)
{
  return ((minimum <= val) && (val <= maximum));
}


void checkColourSensor(){

  //red values
  digitalWrite(S2,LOW);
  digitalWrite(S3,LOW);
  redFreq = pulseIn(sensorOut, LOW);
  redFreq = map(redFreq, 130, 555, 0, 255);
  if (redFreq < 50 && (redFreq < redFreq && redFreq < blueFreq)){
    detectedColour = 0;
  }

  delay(100);

  // Setting Green filtered photodiodes to be read
  digitalWrite(S2,HIGH);
  digitalWrite(S3,HIGH);
  greenFreq = pulseIn(sensorOut, LOW);
  greenFreq = map(greenFreq, 190, 1300, 0, 255);
  if (greenFreq < 50 && (greenFreq < redFreq && greenFreq < blueFreq)){
    detectedColour = 1;
  }

  delay(100);

  // Setting Blue filtered photodiodes to be read
  digitalWrite(S2,LOW);
  digitalWrite(S3,HIGH);
  blueFreq = pulseIn(sensorOut, LOW);
  blueFreq = map(blueFreq, 55, 370, 0, 255);
  if (blueFreq < 50 && (blueFreq < greenFreq && blueFreq < redFreq)){
    detectedColour = 2;
  }

  delay(100);

}

//implements line following PID and adds values to drive
int addLinePidValues(int dir){
    volatile int targetPosition = (blackTape + whiteTape)/2;
    if (dir == 1){ //checks which sensor to read and prepares the mapping values accordingly
      currentPosition = analogRead(FORWARD_SENSOR);
      int mapUpper = 180;
      int mapLower = 90;
    }else{
      currentPosition = analogRead(REVERSE_SENSOR);
      int mapUpper = 90;
      int mapLower = 0;
    }
    error = targetPosition - currentPosition;
    outputSpeed = error * kP;


    if(abs(error) < iZone) {
     integral = (error * kI) + integral;
    } else {
     integral = 0;
    }

    derivative = (error - previousError) * kD;
    previousError = error;

    outputSpeed = error * kP + integral + derivative;

    //For debugging purposes only
    rightMotorSpeed = rightBaseSpeed + outputSpeed; //( + might need to be changed to - )
    leftMotorSpeed = leftBaseSpeed - outputSpeed; //( - might need to be changed to + )

    Serial.print(currentPosition);
    Serial.print("\t");
    Serial.print(error);
    Serial.print("\t");
    Serial.print(outputSpeed);
    Serial.print("\t");
    Serial.print(constrain(rightMotorSpeed, 0, 180));
    Serial.print("\t");
    Serial.print(constrain(leftMotorSpeed, 0, 180));
    Serial.print("\n");

    return outputSpeed;
}


void setup(){
  Serial.begin(9600);
  driveSetPoint = targetPos;
  attachInterrupt(18, ISRleft, RISING);
  attachInterrupt(19, ISRright, RISING);
  pinMode(22,INPUT); // 18 and 22 come from left
  pinMode(23,INPUT); // 19 and 23 come from right
  pinMode(6,INPUT); //enable pin
  servoLeft.attach(4);  // attaches the servo on pin 9 to the servo object
  servoRight.attach(5);  // attaches the servo on pin 9 to the servo object

}

void loop() {

  //will come from state, [0,1,2,3] = [home,r,g,b]
  int colourRequest = colour; //getcancolour::

  //if home request and not at home, reverse motors: else return at home already.
  if (colourRequest == 0){
    if(countLeft > 0 || countRight > 0){ //these values may need to be modified to account for small displacement errors
      dir = -1;
    }else{
      returnValue = 0;
    }
  }else{ //a colour is requested
    checkColourSensor();
    if (colourRequest == detectedColour){
      servoLeft.write(90);
      servoRight.write(90);
      returnValue = colourRequest;
    }else{
      //set drive distance further on
      driveSetPoint += 100;
    }
  }



  //remove this after testing, this stops the motors after counts == 1000
  if (inRange(countLeft, driveSetPoint-10, driveSetPoint+10) || inRange(countRight, driveSetPoint-10, driveSetPoint+10)){
    Serial.print("At distance set point");
    servoLeft.write(90);
    servoRight.write(90);
    returnValue = -1;
  }

  //every 100 millis
  if (sensorMetro.check() == 1){
    int outputSpeed = addLinePidValues(dir);
    servoLeft.write(leftBaseSpeed + outputSpeed);
    servoRight.write(rightBaseSpeed - outputSpeed);
  }

}
