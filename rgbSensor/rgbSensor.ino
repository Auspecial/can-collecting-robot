

/* Arduino Color Sensing Code
*
*/

//blue on : 40  to 2800
//red on : 115 to 4900
//green on : 170 to 1300


#define S0 4
#define S1 5
#define S2 6
#define S3 7
#define sensorOut 8
int frequency = 0;
void setup() {
//change pins if needed
pinMode(S0, OUTPUT);
pinMode(S1, OUTPUT);
pinMode(S2, OUTPUT);
pinMode(S3, OUTPUT);
pinMode(sensorOut, INPUT);

// Setting frequency-scaling to 20%
digitalWrite(S0,HIGH);
digitalWrite(S1,LOW);

Serial.begin(9600);
}
void loop() {
// Setting red filtered photodiodes to be read
digitalWrite(S2,LOW);
digitalWrite(S3,LOW);


// Reading the output frequency
frequency = pulseIn(sensorOut, LOW);
frequency = map(frequency, 130, 555, 0, 255);

// Printing the value on the serial monitor
Serial.print("R= ");//printing name
Serial.print(frequency);//printing RED color frequency
Serial.print(" ");
delay(100);


// Setting Green filtered photodiodes to be read
digitalWrite(S2,HIGH);
digitalWrite(S3,HIGH);


// Reading the output frequency
frequency = pulseIn(sensorOut, LOW);
frequency = map(frequency, 190, 1300, 0, 255);


// Printing the value on the serial monitor
Serial.print("G= ");//printing name
Serial.print(frequency);//printing Green color frequency
Serial.print(" ");
delay(100);


// Setting Blue filtered photodiodes to be read
digitalWrite(S2,LOW);
digitalWrite(S3,HIGH);
// Reading the output frequency
frequency = pulseIn(sensorOut, LOW);
frequency = map(frequency, 55, 370, 0, 255);
// Printing the value on the serial monitor
Serial.print("B= ");//printing name
Serial.print(frequency);//printing Bliue color frequency
Serial.println(" ");
delay(100);
}