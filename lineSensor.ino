void setup() {
  Serial.begin(9600);
}

void loop() {
  // read the input on analog pin 0:
  int sensorValue1 = analogRead(A0);
  // read the input on analog pin 1:
  //int sensorValue2 = analogRead(A1);
  // print out the values you read:
  sensorValue1 = map(sensorValue1, 600, 900, 0.0, 5.0);
  Serial.print(sensorValue1);
  Serial.print("    ");
  //Serial.println(sensorValue2);
  // delay in between reads for stability
  delay(200);
}