# ENGG200 Robot Stuff #
**Simple webserver - need arduino ethernet shield.**

https://www.arduino.cc/en/Tutorial/WebServer




**Webserver Plan**

Code > Arduino > Ethernet Shield > Router > PC

**Bluetooth Plan**

Arduino Uno > Bluetooth Adaptor? > Arduino Mega

**Things to buy/acquire**

- Arduino Ethernet shield | https://store.arduino.cc/usa/arduino-ethernet-shield-2
- Cheap router
- Ethernet cable
- Bluetooth adaptors x 2 | https://www.ebay.com.au/itm/HC-06-Bluetooth-RS232-Serial-Module-4-Pin-Arduino-AU-Stock-/181711571715